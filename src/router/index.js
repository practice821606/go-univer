import { createRouter, createWebHistory } from 'vue-router';
import Home from '../views/Home.vue';
import WebDevelopment from '../views/WebDevelopment.vue';
import MobileApps from '../views/MobileApps.vue';
import TelegramBots from '../views/TelegramBots.vue';

const routes = [
    { path: '/', component: Home },
    { path: '/web-development', component: WebDevelopment },
    { path: '/mobile-apps', component: MobileApps },
    { path: '/telegram-bots', component: TelegramBots }
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
});

export default router;
